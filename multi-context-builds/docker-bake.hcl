# docker-bake.dev.hcl
group "default" {
    targets = ["multi-contexts-build", "base"]
}

target "base" {
    dockerfile = "Dockerfile.base"
    tags = ["multi-contexts-build:base"]
}

target "multi-contexts-build" {
    dockerfile = "Dockerfile"
    contexts = {
        base = "target:base"
        src1 = "src1"
        src2 = "src2"
    }
    tags = ["multi-contexts-build:main"]
}