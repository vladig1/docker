### How to build a docker image using multiple contexts in the build process?
* You could pass the contexts in the build command:
```
docker buildx build --tag multi-contexts-build:main --build-context src1=src1 --build-context src2=src2 -f Dockerfile .
```
* You could set a configuration file (e.g., docker-bake.hcl), where you can define:
  - define default images to be built, which will be built by default when running: ``docker buildx bake``
  - define specific docker image builds by setting up targets - 'base', 'multi-contexts-build' - where you can specify settings such as:
    - tags: docker image names and tags
    - dockerfile: file to be used for the building of the target
    - contexts: build contexts to be used in the dockerfile of the target (even contexts that reference another target in the configuration file; e.g., base)

  Using the configuration file we can easily run build commands. E.g.,
  ```
  docker buildx bake
  ```

  ```
  docker buildx bake multi-contexts-build
  ```

  ```
  docker buildx bake -f docker-bake.hcl
  ```

For more details check [Docker BuildX docs](https://github.com/docker/buildx/blob/master/docs/reference/buildx_bake.md).