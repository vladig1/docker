### How to hide your docker login passwords from the `~/.docker/config.json` file using `pass` docker-credential-helper on Debian?

1. Log in to the Debian host with the username you want to use for logging into a Docker container registry
   Note: Logging in to the host with another user and then switching to the user you want to access the Docker container regitry using `pass` as a Credentials Store will result in a failure once you try to generate the GPG key. A workaround is to create a password for the desired user that will be used for logging into Docker container registry (if the user has no password) and then log in to the host with that user using the created password. Example:
   ```bash
   ssh another_user@<host-ip-addr> # log into the Debian host machine via SSH with the user whose password you know and who has `root` privileges
   sudo passwd docker_login_user # add a password for the user you want to use for logging into the Docker container registry and whose password you do not know (if changing the password is ok)
   exit # exit the Debian host machine
   ssh docker_login_user@<host-ip-addr> # log into the Debian host machine with the desired user using the created password
   ```
2. Make sure you have `pass` installed (e.g., `which pass`) or install it via: `apt-get install pass -y`
   <i>Note: You also may have to install: `rng-tools` via `apt-get install rng-tools -y` and run the `rngd -r /dev/urandom` to generate required entropy for the GPG key generation (described below).</i>
3. Configure the `pass` Credentials Store once you are logged in to the Debian host machine with the user that you want to log into some Docker container registry and persist credentials using `pass`
   ```bash
   gpg --full-generate-key # generate a GPG key (choose default settings by pressing `Enter` or `y`; note: you can provide a fake email address)

   mkdir ~/bin && cd ~/bin # create a new directory and go there

   wget https://github.com/docker/docker-credential-helpers/releases/download/v0.6.3/docker-credential-pass-v0.6.3-amd64.tar.gz. # download docker-credential-pass

   tar xvzf docker-credential-pass-v0.6.3-amd64.tar.gz # untar the downloaded file

   chmod a+x docker-credential-pass # give the new file the execute permissions

   cp docker-credential-pass /usr/local/bin # copy the executable (note: `root` privileges may be needed)

   mkdir ~/.docker # create a new directory (if needed)

   gpg --list-secret-keys # Next you must initialize the pass tool. To do this, locate your GPG ID associated with the key you want to use. The output will be similar to:
    # gpg: key 9C30156EA9E02BD9 marked as ultimately trusted
    # gpg: directory '/home/deployment/.gnupg/openpgp-revocs.d' created
    # dgpg: revocation certificate stored as '/home/deployment/.gnupg/openpgp-revocs.d/E57FEB64FEA4F8F0D73C5B0B9C30156EA9E02BD9.rev'
    # public and secret key created and signed.
    # pub   rsa3072 2023-08-28 [SC]
    #     E57FEB64FEA4F8F0D73C5B0B9C30156EA9E02BD9
    # uid                      deployment <deployment@ablera.com>
    # sub   rsa3072 2023-08-28 [E]

   pass init <GPG key> # initialize `pass` credentials store with the generated GPG key (e.g., `pass init E57FEB64FEA4F8F0D73C5B0B9C30156EA9E02BD9` or `pass init 9C30156EA9E02BD9`)
    
   pass insert docker-credential-helpers/docker-pass-initialized-check # create a password for accessing the `pass` credentials store (Note: Creating such a password will create an issue if you plan to use `pass` as part of a CI/CD pipeline because `docker login <registry-name>` will succeed but `docker pull <docker-image-name>` will fail as it will expect you to provide the password to the `pass` credentials store. However, having no password, all passwords will be easily accessed by simply running the pass `show` command.)
   docker login <registry-name> # log into a Docker container registry to check if new setup works
   vi ~/.docker/config.json # check if you have "credsStore": "pass". E.g.:
   # {
   #      "auths": {
   #              "gitlab.alabala.com:443": {},
   #              "registry.alabala.com": {}
   #      },
   #      "credsStore": "pass"
   #  }
   pass # check if your passwords have been saved in the `pass` credentials store. E.g.:
   # Password Store
   # `-- docker-credential-helpers
   #     |-- Z2l0bGFiLmFsYWJhbGEuY29tOjQ0Mwo=
   #     |   `-- vladimir.georgiev
   #     |-- docker-pass-initialized-check
   #     `-- cmVnaXN0cnkuYWxhYmFsYS5jb20K
   #         `-- vladimir.georgiev
   # Here, we can see that the credentials store has to passwords as there are two base64-encoded indices. By base64-decoding each one of them we can find out which Docker container registry they refer to: e.g. `echo "Z2l0bGFiLmFsYWJhbGEuY29tOjQ0Mwo=" | base64 -d` results in `gitlab.alabala.com:443` and the other is for `registry.alabala.com`
   # In order to see the password for `gitlab.alabala.com:443`, you can run this command:
   pass docker-credential-helpers/Z2l0bGFiLmFsYWJhbGEuY29tOjQ0Mwo=/vladimir.georgiev
   # In order to delete or edit the password, run:
   pass rm/ edit docker-credential-helpers/Z2l0bGFiLmFsYWJhbGEuY29tOjQ0Mwo=/vladimir.georgiev
   ```


